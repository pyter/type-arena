# Update the type hints here as good as you can
# before running `python arena.py`
from typing import Hashable
from typing import Sized
from typing import TypeVar


T = TypeVar("T", bound=Hashable)
def count_each_value(input_dict: dict[T, Sized]) -> dict[T, int]:
    return {k: len(v) for k, v in input_dict.items()}
