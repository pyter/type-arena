import subprocess
import sys
import time
from io import StringIO
from pathlib import Path
from time import sleep
from typing import NamedTuple

from mypy.build import BuildManager
from mypy.fscache import FileSystemCache
from mypy.main import run_build
from mypy.modulefinder import BuildSource
from mypy.options import Options

CLI_ARG = "enter_arena"

logo = r"""\
___________                        _____                              
\__    ______.________   ____     /  _  \_______  ____   ____ _____   
  |    | <   |  \____ \_/ __ \   /  /_\  \_  __ _/ __ \ /    \\__  \  
  |    |  \___  |  |_> \  ___/  /    |    |  | \\  ___/|   |  \/ __ \_
  |____|  / ____|   __/ \___  > \____|__  |__|   \___  |___|  (____  /
          \/    |__|        \/          \/           \/     \/     \/ 
"""

class Level(NamedTuple):
    level_no: int
    num_good: int
    num_bad: int

LEVELS = [
    Level(level_no=1, num_good=2, num_bad=2),
]


def wait(secs: float) -> None:
    sleep(secs / 5)

    for _ in range(3):
        print(".", end="")
        sleep(secs / 5)

    print()
    sleep(secs / 5)


def print_wait(text: str, wait_secs: float = 1.) -> None:
    for letter in text:
        print(letter, end="", flush=True)
        sleep(0.03)

    wait(wait_secs)


def run_arena() -> None:
    print(logo)
    print()
    print(" > Welcome to the Type Arena!")
    print_wait("", wait_secs=4)

    if len(sys.argv) <= 1 or sys.argv[1] != CLI_ARG:
        print_intro()
        return

    for lvl in LEVELS:
        res = run_level(lvl)
        if not res:
            print_game_over(lvl.level_no)
            return


def print_intro():
    print_wait(" > Your type hint skills will be tested in multiple levels", 3)
    print_wait(" > To participate in the arena, first, edit the `battle_function.py` file", 3)
    print_wait(" > Add or edit the type hints of the contained function", 3)
    print_wait(" > The type hints should pass as many levels as possible", 3)
    print_wait(" > Each level contains a number of good examples your type hints should accept", 3)
    print_wait(" > ... as well as bad examples that should be rejected", 3)
    print_wait(f" > Once you are ready, run this file again, but add '{CLI_ARG}' as argument", 3)
    print()
    print(f" >>> python arena.py {CLI_ARG}")
    sleep(1)


def run_level(level: Level) -> bool:
    print(f" ===== Level {level.level_no} =====")
    sleep(1.5)
    print(" - Running good examples: ", end="")
    good_success = run_good_examples(level)
    print("PASSED" if good_success else "FAILED")

    sleep(1.5)
    print(" - Running bad examples: ", end="")
    bad_success = run_bad_examples(level)
    print("PASSED" if bad_success else "FAILED")
    sleep(0.5)
    print()
    sleep(0.5)

    return good_success and bad_success


def run_mypy(p: Path) -> BuildManager:
    options = Options()
    options.strict_equality = True
    options.extra_checks = True
    fscache = FileSystemCache()
    t0 = time.time()
    stdout = StringIO()
    stderr = StringIO()
    sources = [BuildSource(path=str(p), module=None, text=None)]

    res, msg, blockers = run_build(
        sources, options, fscache, t0, stdout, stderr
    )

    if not res:
        msg = f"Something went wrong when checking {p}"
        raise RuntimeError(msg)

    return res.manager


def run_good_examples(level: Level) -> bool:
    p = Path.cwd() / f"level{level.level_no}" / "good.py"

    res = run_mypy(p)
    errors = res.errors.error_info_map.get(str(p), [])

    return len(errors) == 0


def run_bad_examples(level: Level) -> bool:
    p = Path.cwd() / f"level{level.level_no}" / "bad.py"

    res = run_mypy(p)
    errors = res.errors.error_info_map.get(str(p), [])

    return len(errors) == level.num_bad


def print_failure(proc: subprocess.CompletedProcess, good: bool) -> None:
    if good:
        print("Some good objects were incorrectly rejected by the type checker...")
    else:
        print("Some bad objects were incorrectly accepted by the type checker...")
    print()
    print(" ### Stdout:")
    print(proc.stdout)
    print()
    print(" ### Stderr:")
    print(proc.stderr)
    print()


def print_game_over(level: int) -> None:
    print("**** GAME OVER ****")
    print(f"You reached but failed level {level}")
    print("Edit the battle function's type hints and try again!")


if __name__ == '__main__':
    run_arena()
