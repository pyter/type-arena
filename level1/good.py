from typing import Any

from battle_function import count_each_value


class A:
    pass


def use_result(d: dict[Any, int]) -> None:
    pass


res = count_each_value({1: [1, 2]})
use_result(res)


_ = count_each_value({A(): []})
